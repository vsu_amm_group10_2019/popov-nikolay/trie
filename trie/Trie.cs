﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace trie
{
    class Trie
    {
        private Node root;

        //конструктор создает узел - корень
        public Trie()
        {
            root = new Node(0);
        }

        //добавление слова в дерево
        public void Add(string word)
        {
            root.Add(word);
        }

        //удаление слова из дерева
        public bool Delete(string word)
        {
            return root.Delete(word);
        }

        //создание дерева в treeview
        public void MakeTree(TreeView tw)
        {
            tw.Nodes.Clear();
            TreeNode node = new TreeNode("");
            foreach (KeyValuePair<char, Node> p in root.subNodes)
            {
                TreeNode n = new TreeNode(p.Key.ToString());
                p.Value.MakeSubTree(n);
                tw.Nodes.Add(n);
            }
            tw.ExpandAll();
        }

        public int Count()
        {
            return root.Count();
        }
    }
}
