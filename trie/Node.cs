﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace trie
{
    class Node
    {
        //признак конца слова (количество слов)
        public int num { get; set; }

        //потомки
        public Dictionary<char, Node> subNodes;

        //конструктор
        public Node(int n)
        {
            num = n;
            subNodes = new Dictionary<char, Node>();
        }

        //создание поддерева в treeview
        public void MakeSubTree(TreeNode treeNode)
        {
            foreach (KeyValuePair<char, Node> p in subNodes)
            {
                TreeNode n = new TreeNode(p.Key.ToString());
                p.Value.MakeSubTree(n);
                treeNode.Nodes.Add(n);
            }
        }

        //удаление слова
        public bool Delete(string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return true;
            }
            char sym = word[0];
            if (subNodes.ContainsKey(sym))
            {
                if (subNodes[sym].Delete(word.Substring(1)) && subNodes[sym].subNodes.Count == 0)
                {
                    subNodes.Remove(sym);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        //добавление слова
        public void Add(string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                num++;
                return;
            }
            if (!subNodes.ContainsKey(word[0]))
            {
                subNodes.Add(word[0], new Node(0));
            }
            subNodes[word[0]].Add(word.Substring(1));
        }

        //подсчет количества
        public int Count()
        {
            int result = 0;
            foreach (KeyValuePair<char, Node> p in subNodes)
            {
                if (p.Value.num != 0 && Consonants.list.Contains(p.Key))
                {
                    result += p.Value.num;
                };
                result += p.Value.Count();
            }
            return result;
        }
    }
}
