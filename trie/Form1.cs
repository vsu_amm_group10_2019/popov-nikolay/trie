﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace trie
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Trie tree = new Trie();

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string[] lines = File.ReadAllLines(openFileDialog1.FileName); 
                foreach (string str in lines)
                {
                    string[] words = str.Split(new[] { ' ', ',', ':', '?', '!' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string word in words)
                    {
                        tree.Add(word.Trim().ToUpper());
                    };
                }
                tree.MakeTree(treeView1);
            }
        }

        private void taskToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Посчитать в trie-дереве количество слов, оканчивающихся на согласную букву", "Условие задачи 19");
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void doTaskToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Количество слов, оканчивающихся на согласную букву: " + tree.Count().ToString());
        }

        private void addWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm addForm = new AddForm();
            addForm.ShowDialog();
            if (!string.IsNullOrEmpty(addForm.word))
            {
                tree.Add(addForm.word.Trim().ToUpper());
                tree.MakeTree(treeView1);
            }
        }

        private void deleteWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm addForm = new AddForm();
            addForm.ShowDialog();
            if (!string.IsNullOrEmpty(addForm.word))
            {
                if (tree.Delete(addForm.word.Trim().ToUpper()))
                {
                    tree.MakeTree(treeView1);
                }
                else
                {
                    MessageBox.Show("Слово не найдено", "Ошибка");
                }
            }
        }
    }
}
